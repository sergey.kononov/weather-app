import Axios from "axios";

const axios = Axios.create({
  baseURL: process.env.REACT_APP_WEATHER_API_URL
});

axios.interceptors.request.use(config => {
  const params = {
    ...config.params,
    appid: process.env.REACT_APP_WEATHER_API_KEY
  };

  return {
    ...config,
    params
  };
});

axios.interceptors.response.use(response => response.data);

export default axios;
