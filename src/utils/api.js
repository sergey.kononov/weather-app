import axios from "./axios";

export const getWeatherByCityName = async name => {
  try {
    return await axios("weather", {
      params: {
        q: name,
        units: "metric"
      }
    });
  } catch (e) {
    throw e.response.data;
  }
};

export const getWeatherByGeoCoord = async (lat, lon) => {
  try {
    return await axios("weather", {
      params: {
        lat,
        lon,
        units: "metric"
      }
    });
  } catch (e) {
    throw e.response.data;
  }
};
