import React, { Component, Fragment } from "react";
import { isEmpty } from "lodash";

import Container from "@material-ui/core/Container";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";

import SearchForm from "./components/SearchForm";
import WeatherInfoTable from "./components/WeatherInfoTable";
import WeatherMap from "./components/WeatherMap";
import Header from "./components/Header";

import { setStorage, getStorage, removeStorage } from "./utils/storage";
import { getWeatherByGeoCoord } from "./utils/api";

import styles from "./App.module.scss";

const STORAGE_KEY = "weatherInfo";

class App extends Component {
  state = {
    weatherInfo: getStorage(STORAGE_KEY)
  };

  componentDidMount() {
    const { weatherInfo } = this.state;

    if (isEmpty(weatherInfo)) {
      this.getCurrentLocation();
    }
  }

  handleGeolocationCallback = async position => {
    const { latitude, longitude } = position.coords;
    const data = await getWeatherByGeoCoord(latitude, longitude);
    this.setWeatherInfo(data);
  };

  setWeatherInfo = value => {
    this.setState(
      {
        weatherInfo: value
      },
      () => {
        if (value) {
          setStorage(STORAGE_KEY, value);
        } else {
          removeStorage(STORAGE_KEY);
        }
      }
    );
  };

  getCurrentLocation = () => {
    const isGeolocationAvailable = "geolocation" in navigator;

    if (isGeolocationAvailable) {
      navigator.geolocation.getCurrentPosition(
        this.handleGeolocationCallback,
        null,
        { enableHighAccuracy: true }
      );
    }
  };

  render() {
    const { weatherInfo } = this.state;

    return (
      <Container maxWidth="sm" className={styles.AppContainer}>
        <AppBar position="static" className={styles.AppBar}>
          <Toolbar>
            <Typography variant="h6" component="h1">
              Weather App
            </Typography>
          </Toolbar>
        </AppBar>

        <Card>
          <CardContent>
            {weatherInfo && (
              <Header
                name={weatherInfo.name}
                getCurrentLocation={this.getCurrentLocation}
              />
            )}

            <SearchForm setWeatherInfo={this.setWeatherInfo} />

            {weatherInfo && (
              <Fragment>
                <WeatherInfoTable data={weatherInfo} />
                <WeatherMap
                  center={[weatherInfo.coord.lat, weatherInfo.coord.lon]}
                  weatherInfo={weatherInfo}
                />
              </Fragment>
            )}
          </CardContent>
        </Card>
      </Container>
    );
  }
}

export default App;
