import React from "react";
import { shallow } from "enzyme";
import WeatherMap from "./WeatherMap";

describe("<WeatherMap />", () => {
  const props = {
    center: [20, 20],
    weatherInfo: {
      name: "London",
      description: "Clear sky",
      sys: { country: "GB" }
    }
  };

  it("Should render without errors", () => {
    const wrapper = shallow(<WeatherMap {...props} />).dive();
    expect(wrapper.exists()).toBeTruthy();
  });
});
