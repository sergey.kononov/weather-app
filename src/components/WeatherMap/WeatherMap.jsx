import React from "react";
import PropTypes from "prop-types";
import { get } from "lodash";

import Typography from "@material-ui/core/Typography";

import { Map as LeafletMap, TileLayer, Marker, Popup } from "react-leaflet";

import withLeafletMap from "../../hocs/withLeafletMap";

import styles from "./WeatherMap.module.scss";

const WeatherMap = ({ center, zoom, url, weatherInfo }) => {
  const {
    name,
    sys: { country },
    weather
  } = weatherInfo;
  const description = get(weather, "[0].description");

  return (
    <div className={styles.Map}>
      <LeafletMap
        center={center}
        zoom={zoom}
        attributionControl
        zoomControl
        doubleClickZoom
        scrollWheelZoom
        dragging
        animate
        easeLinearity={0.35}
      >
        <TileLayer url={url} />
        <Marker position={center}>
          <Popup>
            <Typography variant="body1">
              {name}, {country}
            </Typography>
            <Typography variant="body2">{description}</Typography>
          </Popup>
        </Marker>
      </LeafletMap>
    </div>
  );
};

WeatherMap.defaultProps = {
  zoom: 11
};

WeatherMap.propTypes = {
  center: PropTypes.array.isRequired,
  zoom: PropTypes.number,
  url: PropTypes.string.isRequired,
  weatherInfo: PropTypes.object.isRequired
};

export default withLeafletMap(WeatherMap);
