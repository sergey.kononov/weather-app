import React from "react";
import PropTypes from "prop-types";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Typography from "@material-ui/core/Typography";

import styles from "./WeatherInfoTable.module.scss";

const getTime = timestamp => new Date(timestamp * 1000).toLocaleTimeString();

const generateWeatherInfoArray = ({ main, sys: { sunrise, sunset } }) => [
  {
    name: "Temperature",
    value: `${main.temp} ${"\u00b0"}C`
  },
  {
    name: "Pressure",
    value: `${main.pressure} hpa`
  },
  {
    name: "Humidity",
    value: `${main.humidity} %`
  },
  {
    name: "Sunrise",
    value: getTime(sunrise)
  },
  {
    name: "Sunset",
    value: getTime(sunset)
  }
];

const WeatherInfoTable = ({ data }) => (
  <div className={styles.WeatherInfoContainer}>
    <Typography variant="h6" align="center">
      Weather in {data.name}, {data.sys.country}
    </Typography>

    <Table>
      <TableBody>
        {generateWeatherInfoArray(data).map(row => (
          <TableRow key={row.name}>
            <TableCell component="th" scope="row">
              {row.name}
            </TableCell>
            <TableCell align="right">{row.value}</TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  </div>
);

WeatherInfoTable.propTypes = {
  data: PropTypes.object.isRequired
};

export default WeatherInfoTable;
