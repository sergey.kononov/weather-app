import React from "react";
import { shallow } from "enzyme";
import Typography from "@material-ui/core/Typography";
import TableRow from "@material-ui/core/TableRow";

import WeatherInfoTable from "./WeatherInfoTable";

describe("<WeatherInfoTable />", () => {
  const props = {
    data: {
      name: "London",
      description: "Clear sky",
      sys: { country: "GB", sunrise: 1569650132, sunset: 1569692820 },
      main: {
        temp: 20,
        pressure: 1008,
        humidity: 40
      }
    }
  };

  it("Should render without errors", () => {
    const wrapper = shallow(<WeatherInfoTable {...props} />);
    expect(wrapper.exists()).toBeTruthy();
    expect(wrapper.find(Typography).text()).toEqual("Weather in London, GB");
    expect(wrapper.find(TableRow).length).toEqual(5);
  });
});
