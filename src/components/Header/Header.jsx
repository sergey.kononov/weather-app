import React from "react";
import PropTypes from "prop-types";

import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import MyLocation from "@material-ui/icons/MyLocation";
import NearMeIcon from "@material-ui/icons/NearMe";

import styles from "./Header.module.scss";

const Header = ({ name, getCurrentLocation }) => {
  return (
    <Grid container spacing={2} alignItems="center" justify="space-between">
      <Grid item>
        <Typography className={styles.LocationInfo}>
          <MyLocation />
          <span>Chosen location: </span>
          <b>{name}</b>
        </Typography>
      </Grid>

      <Grid item>
        <Button size="small" onClick={getCurrentLocation}>
          <NearMeIcon />
          Current location
        </Button>
      </Grid>
    </Grid>
  );
};

Header.propTypes = {
  name: PropTypes.string.isRequired,
  getCurrentLocation: PropTypes.func.isRequired
};

export default Header;
