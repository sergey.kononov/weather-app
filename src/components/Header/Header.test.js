import React from "react";
import { shallow } from "enzyme";
import Button from "@material-ui/core/Button";
import Header from "./Header";

describe("<Header />", () => {
  const getCurrentLocationMock = jest.fn();

  const props = {
    name: "London",
    getCurrentLocation: getCurrentLocationMock
  };

  it("Should render without errors", () => {
    const wrapper = shallow(<Header {...props} />);
    expect(wrapper.exists()).toBeTruthy();
  });

  it("Should call props method after click", () => {
    const wrapper = shallow(<Header {...props} />);
    wrapper.find(Button).simulate("click");

    expect(getCurrentLocationMock).toHaveBeenCalled();
  });
});
