import React, { Component } from "react";
import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

import { getWeatherByCityName } from "../../utils/api";

import styles from "./SearchForm.module.scss";

class SearchForm extends Component {
  static propTypes = {
    setWeatherInfo: PropTypes.func.isRequired
  };

  state = {
    value: "",
    errorMessage: ""
  };

  onChange = e => {
    const { value } = e.target;

    this.setState({
      value
    });
  };

  search = async e => {
    e.preventDefault();

    const { setWeatherInfo } = this.props;

    try {
      const { value } = this.state;

      const data = await getWeatherByCityName(value);
      setWeatherInfo(data);

      this.setState({
        value: "",
        errorMessage: ""
      });
    } catch (error) {
      console.log(error);

      this.setState({
        errorMessage: error.message
      });

      setWeatherInfo(null);
    }
  };

  render() {
    const { value, errorMessage } = this.state;

    return (
      <form noValidate autoComplete="off" onSubmit={this.search}>
        <Grid container spacing={2} alignItems="center">
          <Grid item xs={8}>
            <TextField
              id="name"
              label="Your city name"
              margin="normal"
              fullWidth
              variant="outlined"
              value={value}
              onChange={this.onChange}
            />
          </Grid>
          <Grid item xs={4}>
            <div className={styles.SearchButton}>
              <Button
                type="submit"
                variant="contained"
                color="primary"
                fullWidth
                size="large"
                disabled={!value.length}
              >
                Search
              </Button>
            </div>
          </Grid>
        </Grid>

        <Grid>
          <Grid item xs={12}>
            {errorMessage && (
              <Typography className={styles.ErrorMessage}>
                {errorMessage}
              </Typography>
            )}
          </Grid>
        </Grid>
      </form>
    );
  }
}

export default SearchForm;
