import React from "react";
import { shallow } from "enzyme";
import TextField from "@material-ui/core/TextField";

import SearchForm from "./SearchForm";

describe("<SearchForm />", () => {
  const setWeatherInfoMock = jest.fn();
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<SearchForm setWeatherInfo={setWeatherInfoMock} />);
  });

  it("Should render without errors", () => {
    expect(wrapper.exists()).toBeTruthy();
  });

  it("Should change value in state", () => {
    wrapper.find(TextField).simulate("change", { target: { value: "London" } });
    expect(wrapper.state().value).toEqual("London");
  });

  it("Should call search method", async () => {
    wrapper.setState({ value: "London" });
    const wrapperInstance = wrapper.instance();

    await wrapperInstance.search({
      preventDefault: jest.fn()
    });

    expect(setWeatherInfoMock).toHaveBeenCalled();
  });

  it("Should call with error", async () => {
    wrapper.setState({ value: "wwwwwww2222" });
    const wrapperInstance = wrapper.instance();

    await wrapperInstance.search({
      preventDefault: jest.fn()
    });

    expect(setWeatherInfoMock).toHaveBeenCalled();
    expect(wrapper.state().errorMessage).toEqual("city not found");
  });
});
