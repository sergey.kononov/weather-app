import React from "react";
import { shallow } from "enzyme";
import withLeafletMap from "./withLeafletMap";
import WeatherMap from "../components/WeatherMap";

describe("withLeafletMap", () => {
  it("Should call without errors and correct props", () => {
    const Component = withLeafletMap(WeatherMap);

    const wrapper = shallow(
      <Component center={[20, 20]} weatherInfo={{ name: "London" }} />
    );
    expect(wrapper.props().url).toBeTruthy();
  });
});
