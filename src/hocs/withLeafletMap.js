import React from "react";
import L from "leaflet";

import icon from "leaflet/dist/images/marker-icon.png";
import iconRetina from "leaflet/dist/images/marker-icon-2x.png";
import iconShadow from "leaflet/dist/images/marker-shadow.png";

import "leaflet/dist/leaflet.css";

// eslint-disable-next-line no-underscore-dangle
delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
  iconRetinaUrl: iconRetina,
  iconUrl: icon,
  shadowUrl: iconShadow
});

const URL = process.env.REACT_APP_LEAFLET_API_URL;

const withLeafletMap = Component => {
  const WithLeafletMap = props => {
    return <Component {...props} url={URL} />;
  };

  return WithLeafletMap;
};

export default withLeafletMap;
