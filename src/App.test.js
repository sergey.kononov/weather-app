import React from "react";
import { shallow } from "enzyme";
import App from "./App";
import WeatherMap from "./components/WeatherMap";

describe("<App /> without Geolocation support", () => {
  it("Should render without errors", () => {
    const wrapper = shallow(<App />);
    expect(wrapper.exists()).toBeTruthy();
  });
});

describe("<App />", () => {
  beforeEach(() => {
    const mockFunction = jest.fn().mockImplementationOnce(success =>
      Promise.resolve(
        success({
          coords: {
            latitude: 64,
            longitude: 26
          }
        })
      )
    );

    global.navigator.geolocation = {
      getCurrentPosition: mockFunction,
      watchPosition: mockFunction,
      clearWatch: jest.fn()
    };
  });

  it("Should render without errors", () => {
    const wrapper = shallow(<App />);
    expect(wrapper.exists()).toBeTruthy();
  });

  it("Should render weatherInfo from localStorage", () => {
    localStorage.setItem(
      "weatherInfo",
      JSON.stringify({ name: "London", coord: { lat: 54.6, lon: 25.3 } })
    );
    const wrapper = shallow(<App />);
    expect(wrapper.find("Header").exists()).toBeTruthy();
    expect(wrapper.find("WeatherInfoTable").exists()).toBeTruthy();
    expect(wrapper.find(WeatherMap).props().center).toEqual([54.6, 25.3]);
  });

  it("Should save or remove from localStorage", () => {
    const wrapper = shallow(<App />);
    const wrapperInstance = wrapper.instance();

    wrapperInstance.setWeatherInfo({
      name: "London",
      coord: { lat: 54.6, lon: 25.3 }
    });
    expect(localStorage.getItem("weatherInfo")).toBeTruthy();

    wrapperInstance.setWeatherInfo(null);
    expect(localStorage.getItem("weatherInfo")).toBeFalsy();
  });

  it("Should get from geolocation", async () => {
    const wrapper = shallow(<App />);
    const wrapperInstance = wrapper.instance();

    await wrapperInstance.handleGeolocationCallback({
      coords: {
        latitude: 64,
        longitude: 26
      }
    });

    expect(wrapper.find(WeatherMap).props().center).toEqual([64, 26]);
  });
});
